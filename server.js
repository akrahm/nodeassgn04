const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const csrf = require("csurf");
const flash = require("connect-flash");
const dbURI = `mongodb+srv://root:12345@cluster0.w93q1.mongodb.net/job-portal?retryWrites=true&w=majority`;
const app = express();
const store = new MongoDBStore({
  uri: dbURI,
  collection: "sessions",
});
const csrfProtection = csrf();
const PORT = 8080;
app.set("view engine", "ejs");
app.set("views", "views");
//const sequelize = require("./util/database");
const adminRoutes = require("./routes/admin");
const signUpRoutes = require("./routes/auth");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
app.use(csrfProtection);
app.use(flash());
app.use((req, res, next) => {
   res.locals.isAuthenticated = req.session.isLoggedIn;
   res.locals.csrfToken = req.csrfToken();
   next();
});
app.use("/admin", adminRoutes);
app.use(signUpRoutes);
app.get("/about-us", (req, res, next) => {
  res.render("about-us", {
    pageTitle: "About Us",
    path: "about-us",
  });
});
app.get("/", (req, res, next) => {
  res.render("index", {
    pageTitle: "Welcome",
    path: "/",
  });
});
(async () => {
  try {
    await mongoose.connect(dbURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    // await sequelize.sync();
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  } catch (err) {
    console.log("error: " + err);
  }
})();