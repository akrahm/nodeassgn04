const path = require("path");

const express = require("express");

const adminController = require("../controllers/admin");

const router = express.Router();

router.post(
  "/create-company",
  adminController.validate("getCreatecompany"),
  adminController.getCreatecompany
);

router.get("/get-companies", adminController.getcompanies);

router.put(
  "/update-company/:companyId",
  adminController.validate("getCreatecompany"),
  adminController.getUpdatecompany
);

router.delete("/delete-company/:companyId", adminController.postDeletecompany);

// user route

router.post(
  "/add-user",
  adminController.validate("postCreateUser"),
  adminController.postCreateUser
);

router.put(
  "/update-user/:userId",
  adminController.validate("postCreateUser"),
  adminController.getUpdateUser
);

router.get("/get-users", adminController.getAllUsers);


router.delete("/remove-user/:userId", adminController.postDeleteUser);


module.exports = router;
