const Company = require("../models/company");
const User = require("../models/user");

const { validationResult } = require("express-validator");

exports.getCreatecompany = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

  if (!errors.isEmpty()) {
    return res.send({
      status: "false",
      error: errors.array(),
    });
  }
  const { name, url, location, description } = req.body;
  const _company = new Company({
    name,
    url,
    location,
    description,
  });

  _company.save((error, data) => {
    if (error) {
      return res.status(400).json({
        message: "Something went wrong",
      });
    } else {
      return res.status(201).json({
        company: data,
      });
    }
  });
};

exports.getUpdatecompany = async (req, res, next) => {
  const compId = req.body.companyId;
  const { name, url, location, description } = req.body;
  try {
    const _company = await Company.findById(compId);
    _company.name = name;
    _company.url = url;
    _company.location = location;
    _company.description = description;
    _company.save((error, data) => {
      if (error) {
        return res.status(400).json({
          message: "Something went wrong",
        });
      } else {
        return res.status(201).json({
          message: "update successfully",
          company: data,
        });
      }
    });
  } catch (error) {
    return res.status(400).json({
      error: error,
    });
  }
};

exports.getcompanies = async (req, res, next) => {
  try {
    const companies = await Company.find({});
    return res.send({
      success: "true",
      companies,
    });
  } catch (error) {
    return res.send({
      success: "false",
      message: "something went wrong",
    });
  }
};

exports.postDeletecompany = async (req, res, next) => {
  const compId = req.body.companyId;
  try {
    const result = await Company.findByIdAndRemove(compId);

    res.send({
      success: "true",
      message: "Company deleted Successfully",
    });
  } catch (error) {
    res.send({
      success: "false",
      message: error,
    });
  }
};
exports.postCreateUser = async (req, res) => {
  try {
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

    if (!errors.isEmpty()) {
      return res.send({
        status: "false",
        error: errors.array(),
      });
    }
    const { firstName, lastName, email, phone, password} = req.body;

    const user = await User.create({
      firstName,
      lastName,
      email,
      phone,
      password
    });

    return res.send({
      status: "true",
      message: "user created Succesfully",
      user: user,
    });
  } catch (err) {
    return res.send({
      status: "false",
      error: err,
    });
  }
};

exports.getUpdateUser = async (req, res, next) => {
  const id = req.body.userId;
  const { firstName, lastName, email, phone } = req.body;
  console.log(id);
  if (firstName && phone && email && id) {
    try {
      const result = await User.update(
        { firstName, lastName, email, phone },
        { where: { id } }
         );
        
      return res.send({
        status: "true",
        message: result,
      });
    } catch (err) {
      console.log(err);
      return res.send({
        status: "false",
        message: err,
      });
    }
  } else {
    return res.send({
      status: "false",
      message: "first Name,email,phone is required",
    });
  }
};
exports.getAllUsers = (req, res, next) => {
  User.findAll().then((result) => {
    return res.send({
      success: "true",
      users: result,
    });
  });
};
exports.postDeleteUser = async (req, res, next) => {
  const id = req.body.userId;
  console.log(id);
  try {
    await User.destroy({
      where: {id},
    });
    return res.send({
      success: "true",
      message: "Deleted successfully",
    });
  } catch (error) {
    return res.send({
      success: "false",
      message: error
    });
  }
};

// validation
const { body } = require("express-validator/check");

exports.validate = (method) => {
  switch (method) {
    case "postCreateUser": {
      return [
        body("firstName", "userName doesn't exists").exists(),
        body("email", "Invalid email").exists().isEmail(),
        body("phone").exists().isInt(),
      ];
    }
    case "getCreatecompany": {
      return [
        body("name", "userName doesn't exists").exists(),
        body("url", "Url doesn't exist").exists(),
        body("location", "location doesn't exists").exists(),
      ];
    }
  }
};
